const express=require('express');
const bodyparser=require('body-parser');
const cors=require('cors');
const routing=require('./routes');
const app=express();

app.use(cors());
app.use(bodyparser.json());
app.use('/',routing);

app.listen(8095,'localhost',err=>{
    if(err){
        console.log(err);
        process.exit(-1);
    }
    console.log('connected to localhost at post 8095');
});
