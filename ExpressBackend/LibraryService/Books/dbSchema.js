const mongoose=require('mongoose');
const schema=mongoose.Schema;

const bookSchema=new schema({
    Name:{type:String,required:true},
    ISBN:{type:String,required:true},
    Author:{type:String,required:true},
    Price:{type:String,required:true},
    Year:{type:String,required:true},
    Publisher:{type:String,required:true}

});

const authorSchema=new schema({
    fname:{type:String,required:true},
    lname:{type:String,required:true},
    nationality:{type:String,required:true}
});

mongoose.model('book',bookSchema,'booklist');
mongoose.model('author',authorSchema,'authorlist');

mongoose.connect('mongodb://localhost:27017/afFinal',(err)=>{
    if(err){
        console.log(err);
        process.exit(-1);
    }
});

module.exports=mongoose;