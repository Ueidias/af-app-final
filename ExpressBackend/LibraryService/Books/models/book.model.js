var schema=require('../dbSchema');
var bookmodel=schema.model('book');

exports.getBooks = function (req,res) {
    bookmodel.find().exec().then((data)=>{
        res.send(data)
    }).catch((err)=>{
        console.log(err);
    });
};

exports.getBook = function (req,res) {
    var bkName=req.params.Author;
    bookmodel.find({Name:bkName}).exec().then((data)=>{
        res.send(data)
    }).catch((err)=>{
        console.log(err);
    });
};

exports.getAutBooks = function (req,res) {
    bookmodel.find({},'Author').then((data)=>{
        res.send(data);
    }).catch((err)=>{
        console.log(err);
    });
};

exports.deleteBooks=function(req,res){
    var bkName=req.params.name;
    bookmodel.remove({Name:bkName}).exec().then((data)=>{
        res.status(200);
        console.log(bkName);
    }).catch((err)=>{
        console.log(err);
    });
};

exports.addBooks=function(req,res){
    var newBook=new bookmodel({
        Name:req.body.Name,
        ISBN:req.body.ISBN,
        Author:req.body.Author,
        Price:req.body.Price,
        Year:req.body.Year,
        Publisher:req.body.Publisher
    });
    newBook.save().then(()=>{
        res.status(201);
    }).catch((err)=>{
        console.log(err);
    });
};