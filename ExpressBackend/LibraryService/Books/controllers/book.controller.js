const express=require('express');
const router=express.Router();
const bookModel=require('../models/book.model');

router.get('/',bookModel.getBooks);
router.get('/authors',bookModel.getAutBooks);
router.get('/:name',bookModel.getBook);
router.post('/',bookModel.addBooks);
router.delete('/:name',bookModel.deleteBooks);

module.exports=router;