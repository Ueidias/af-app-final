const express=require('express');
const router=express.Router();
const authorModel=require('../models/author.model');

router.get('/',authorModel.getAuthors);
module.exports=router;