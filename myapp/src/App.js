import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import ProductItem from "./ProductItem";
import AddItem from "./AddItem";
import axios from 'axios';


class App extends Component {

    constructor(props){
        super(props);

        this.state={
            books:[],
            authors:[]
        };
        this.onAdd=this.onAdd.bind(this);
        this.onDelete=this.onDelete.bind(this);
    }

    componentWillMount(){
        axios.get('http://localhost:8095/books')
            .then(res=>{
                const books=res.data;
                this.setState({books});
            });

        axios.get('http://localhost:8095/authors')
            .then(res=>{
                const authors=res.data;
                this.setState({authors});
            })
    }

    getBooks(){
        return this.state.books;
    }

    onAdd(Name,ISBN,Author,Price,Year,Publisher){
        const books=this.getBooks();
        books.push({
            Name,
            ISBN,
            Author,
            Price,
            Year,
            Publisher
        });
        this.setState({books});

        axios.post(`http://localhost:8095/books`, {
            Name:Name,
            ISBN:ISBN,
            Author:Author,
            Price:Price,
            Year:Year,
            Publisher:Publisher
        }).then(res => {
            console.log(res);
            console.log(res.data);
        })
    }

    onDelete(Name){
        const books=this.getBooks();
        const filteredproducts=books.filter(book=>{
            return book.Name !==Name;
        });

        this.setState({books:filteredproducts})
        console.log(filteredproducts);

        axios.delete(`http://localhost:8095/books/${Name}`).then(res => {
            console.log(res);
            console.log(res.data);
        })
    }

    render() {
        return (
            <div className="App">
                <header className="">
                    <img src={logo} className="App-logo" alt="logo" />
                </header>
                <h1>Library</h1>
                <table>
                    <tbody>
                    <tr>
                        <td>
                            <AddItem
                                onAdd={this.onAdd}
                                authors={this.state.authors}
                            />
                        </td>
                        <td>

                        </td>
                        <td className="App-Body">
                            <h3>Book list</h3>
                            {
                                this.state.books.map(product=>{
                                    return(
                                        <ProductItem
                                            key={product._id}
                                            Name={product.Name}
                                            ISBN={product.ISBN}
                                            Author={product.Author}
                                            Price={product.Price}
                                            Year={product.Year}
                                            Publisher={product.Publisher}
                                            onDelete={this.onDelete}
                                        />
                                    );
                                })
                            } </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        );
    }
}

export default App;
