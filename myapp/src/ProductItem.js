import React, { Component } from 'react';

class ProductItem extends Component {
constructor(props){
    super(props);
    this.onDelete=this.onDelete.bind(this);
}

onDelete(){
    const  {onDelete,Name}=this.props;
    onDelete(Name);
}

    render() {
        const {Name,ISBN,Author,Price,Year,Publisher}=this.props;

        return(
            <div className="App-Body">
                <span>Name:{Name}</span>
                {`|`}
                <span>ISBN:{ISBN}</span>
                {`|`}
                <span>Author:{Author}</span>
                {`|`}
                <span>Price:{Price}</span>
                {`|`}
                <span>Year:{Year}</span>
                {`|`}
                <span>Publisher:{Publisher}</span>
                {`|`}
                <button onClick={this.onDelete}>Delete</button>
            </div>
        );
    }
}

export default ProductItem;
